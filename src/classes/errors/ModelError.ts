import type { Details } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';
import {
  Model,
  ModelMethodError,
} from '@avstantso/node-or-browser-js--model-core';

import { MESSAGES, METHODS } from '../../consts';

export namespace ModelError {
  export type ErrorCallback = (error: Error) => PromiseLike<never>;
  export type Checker = {
    (
      method: string,
      details: Details,
      message: string,
      ...altMessages: string[]
    ): ErrorCallback;

    /**
     * @summary Select or ById method, depend of id parameter
     */
    select(
      id: Model.ID,
      details: Details,
      message: string,
      ...altMessages: string[]
    ): ErrorCallback;
  };
}

const quotedValueRegEx = /(?<=').+(?=')/;
const aPrefixRegEx = /^a_/;

export class ModelError extends ModelMethodError {
  constructor(
    model: string,
    method: string,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, ModelError.prototype);
  }

  static is(error: unknown): error is ModelError {
    return JS.is.error(this, error);
  }

  static check(
    model: string,
    method: string,
    details: Details,
    message: string,
    ...altMessages: string[]
  ): ModelError.ErrorCallback {
    return (error: Error): PromiseLike<never> => {
      if (
        message === error.message ||
        (altMessages.length > 0 &&
          altMessages.find((am) => am === error.message))
      ) {
        const e = new ModelError(model, method, error.message, error);
        e.details = details;
        throw e;
      }

      if (error.message.startsWith(MESSAGES.cannotDeleteOrUpdateAParentRow)) {
        const e = new ModelError(
          model,
          method,
          MESSAGES.attemptDataConsistencyViolationPrevented,
          error
        );
        e.details = details;
        throw e;
      }

      if (error.message.startsWith(MESSAGES.dataTooLongForColumn)) {
        const [paramName] = quotedValueRegEx.exec(error.message);

        const e = new ModelError(
          model,
          method,
          MESSAGES.dataTooLongForColumn,
          error
        );
        e.details = {
          ...details,
          errorColumnName: paramName.replace(aPrefixRegEx, ''),
        };
        throw e;
      }

      throw error;
    };
  }

  static checker(model: string): ModelError.Checker {
    const f: ModelError.Checker = (method, details, message, ...altMessages) =>
      ModelError.check(model, method, details, message, ...altMessages);

    f.select = (id, details, message, ...altMessages) =>
      ModelError.check(
        model,
        `${!id ? METHODS.select : METHODS.byId}`,
        details,
        message,
        ...altMessages
      );

    return f;
  }

  // for tests
  static fail(): never {
    throw Error(MESSAGES.noModelError);
  }
}
