import type { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  ColDefConverter,
  ViewFields,
  View,
} from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import Factories from './factories';

const Make =
  <
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Modeler.Access.Union<TModel>
  >(
    factoryOptions: Modeler.Factories.Options.IdFromStr &
      Modeler.Factories.Options.Detail
  ) =>
  (options: Modeler.Factories.Options.Common<TModel, TModeler, TAccess>) => {
    function Options<TExtOptions>(extOptions: TExtOptions) {
      const { isSafe, errorHandler } = options;

      const safe = <T>(
        promise: Promise<T>,
        filter?: (e: Error) => boolean
      ): Promise<T> =>
        promise.catch((e) => {
          if (isSafe && filter && filter(e)) return undefined;

          if (errorHandler) return errorHandler(e);

          throw e;
        });

      return {
        ...factoryOptions,
        ...options,
        safe,
        ...extOptions,
      };
    }

    return {
      FromView: <TViewFields extends ViewFields>(view: View<TViewFields>) =>
        Factories.View<TViewFields, TModel, TModeler, TAccess>(
          Options({
            view,
          })
        ),
      FromProc: (cdcRules?: ColDefConverter.Rule | ColDefConverter.Rule[]) =>
        Factories.Proc<TModel, TModeler, TAccess>(
          Options({
            cdcRules,
          })
        ),
    };
  };

export default function (factoryOptions: Modeler.Factories.Options.IdFromStr) {
  const { idFromStr } = factoryOptions;

  const Top = <
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel> = Modeler.Structure<TModel>
  >(
    options: Modeler.Factories.Options.Common<TModel, TModeler, Modeler.Access>
  ) =>
    Make<TModel, TModeler, Modeler.Access>({ idFromStr, isDetail: false })(
      options
    );

  const Detail = <
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel> = Modeler.Structure<TModel>
  >(
    options: Modeler.Factories.Options.Common<
      TModel,
      TModeler,
      Modeler.Access.Detail<TModel>
    >
  ) =>
    Make<TModel, TModeler, Modeler.Access.Detail<TModel>>({
      idFromStr,
      isDetail: true,
    })(options);

  return {
    Top,
    Detail,
  };
}
