import Bluebird from 'bluebird';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Arg } from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import { METHODS } from '@consts';
import { ModelError } from '@classes';
import { logMethod } from '@utils';
import Writable from '../writable';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.Proc.Structure<
    TModel,
    TAccess
  > = Modeler.Factories.Proc.Structure<TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.Proc<TModel, TModeler, TAccess>
): Modeler.Factories.Proc.Methods.WritableNamed<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const writableFactory = Writable<TModel, TModeler, TAccess>(factoryOptions);
  const { nameToId, isDetail, safe } = factoryOptions;

  return (
    insertProc,
    updateProc,
    deleteProc,
    deleteByAuthorProc,
    byId,
    byName
  ) => {
    const writable = writableFactory(
      insertProc,
      updateProc,
      deleteProc,
      deleteByAuthorProc,
      byId
    );

    return (access) => {
      const w = writable(access);

      const deleteByName: Modeler.Methods.Writable.Delete.ByName<
        TModel,
        TModeler
      > = (name): Promise<Model.ID> =>
        safe(
          logMethod.async(METHODS.deleteByName).then(() => {
            return Bluebird.resolve<Arg<any>>(
              nameToId
                ? nameToId({ access, name })
                : byName(access)(name).then(({ id }) => id)
            ).then((id) => id && w.delete(id));
          }),
          // There is no such thing, okay
          ModelError.is
        );

      return { ...w, deleteByName };
    };
  };
}
