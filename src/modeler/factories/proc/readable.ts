import type { ResultSetHeader, RowDataPacket } from 'mysql2/promise';
import Bluebird from 'bluebird';
import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { ColDefConverter, Func } from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import { METHODS } from '@consts';
import { logMethod, oneRowProc } from '@utils';
import Extender from '../extender';
import { ModelError } from '@classes';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.Proc.Structure<
    TModel,
    TAccess
  > = Modeler.Factories.Proc.Structure<TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.Proc<TModel, TModeler, TAccess>
): Modeler.Factories.Base.Methods.Readable<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  //#region internal types aliases
  type Select = TModel['Select'];
  type SelectArray = Select[];
  type PGResult = Model.Pagination.Result<TModel>;
  //#endregion

  const {
    modelName,
    executor,
    decorator,
    idFromStr,
    isDetail,
    safe,
    cdcRules,
  } = factoryOptions;
  const { call } = executor;

  return (selection) => {
    const factory: Modeler.Access.For<
      Modeler.Readable<TModel, TModeler>,
      TModel,
      TAccess
    > = (access) => {
      const select: Modeler.Methods.Readable.Select<TModel, TModeler> = (
        search?
      ): Promise<SelectArray> =>
        safe(
          logMethod.async(METHODS.select).then(() =>
            call<RowDataPacket[][]>(
              selection({ access, ...(search ? { search } : {}) })
            )
              .then(ColDefConverter(cdcRules))
              .then(([rowsP]) => {
                const rows = rowsP[0] as SelectArray;
                return decorator ? rows.map(decorator) : rows;
              })
          )
        );

      const selectPages: Modeler.Methods.Readable.Select<
        TModel,
        TModeler
      >['pages'] = (pagination, search?): Promise<PGResult> =>
        safe(
          logMethod.async(METHODS.select.pages).then(() => {
            return call<RowDataPacket[][]>(
              selection({ access, pagination, ...(search ? { search } : {}) })
            )
              .then(ColDefConverter(cdcRules))
              .then(([pagesRowsP]) => {
                if (!Array.isArray(pagesRowsP) || 3 !== pagesRowsP.length) {
                  // console.warn('pagesRowsP: %O', pagesRowsP);
                  throw new ModelError(
                    modelName,
                    METHODS.select.pages,
                    `Pages rows packet must be array with length 2: [total query, rows query, mysql ResultSetHeader]`
                  );
                }

                const [totalP, rowsP, resultSet]: [
                  NodeJS.Dict<number>[],
                  SelectArray,
                  ResultSetHeader
                ] = Generics.Cast.To(pagesRowsP);

                if (!Array.isArray(totalP) || 1 !== totalP.length) {
                  // console.warn('totalP: %O', totalP);
                  throw new ModelError(
                    modelName,
                    METHODS.select.pages,
                    `Total packet must be array with length 1`
                  );
                }

                const [total]: [number] = Generics.Cast.To(
                  Object.values(totalP[0])
                );

                if (!JS.is.number(total))
                  throw new ModelError(
                    modelName,
                    METHODS.select.pages,
                    `Total value must be number but it's ${typeof total}`
                  );

                if (!Array.isArray(rowsP))
                  throw new ModelError(
                    modelName,
                    METHODS.select.pages,
                    `Rows packet must be array`
                  );

                return [total, decorator ? rowsP.map(decorator) : rowsP];
              });
          })
        );

      select.pages = selectPages;

      const byId: Modeler.Methods.Readable.ById<TModel, TModeler> = (id) =>
        safe(
          logMethod
            .async(METHODS.byId)
            .then(() => {
              return call<RowDataPacket[][]>(
                selection({
                  access,
                  id: id instanceof Func ? id : idFromStr(id),
                })
              );
            })
            .then(ColDefConverter(cdcRules))
            .then(oneRowProc<Select>)
            .then((row) => (decorator ? decorator(row) : row))
        );

      return Extender(factoryOptions, access, factory, {
        name: modelName,
        isDetail,
        select,
        byId,
      });
    };

    return factory;
  };
}
