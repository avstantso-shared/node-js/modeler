import type { RowDataPacket } from 'mysql2/promise';
import Bluebird from 'bluebird';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { ColDefConverter } from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import { METHODS } from '@consts';
import { logMethod, oneRowProc } from '@utils';
import Extender from '../extender';
import Readable from './readable';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.Proc.Structure<
    TModel,
    TAccess
  > = Modeler.Factories.Proc.Structure<TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.Proc<TModel, TModeler, TAccess>
): Modeler.Factories.Base.Methods.ReadableNamed<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const { extend, ...internalOptions } = factoryOptions;
  const readableFactory = Readable<TModel, TModeler, TAccess>(internalOptions);

  const { executor, decorator, nameToId, safe, cdcRules } = factoryOptions;
  const { call } = executor;

  return (selection) => {
    const readable = readableFactory(selection);

    const factory: Modeler.Access.For<
      Modeler.ReadableNamed<TModel, TModeler>,
      TModel,
      TAccess
    > = (access) => {
      const r = readable(access);

      const byName: Modeler.Methods.Readable.ByName<TModel, TModeler> = (
        name
      ): Promise<TModel['Select']> =>
        safe(
          logMethod
            .async(METHODS.byName)
            .then(() =>
              call<RowDataPacket[][]>(
                selection({
                  access,
                  ...(nameToId ? { id: nameToId({ access, name }) } : { name }),
                })
              )
            )
            .then(ColDefConverter(cdcRules))
            .then(oneRowProc<TModel['Select']>)
            .then((row) => (decorator ? decorator(row) : row))
        );

      return Extender(extend, access, factory, { ...r, byName });
    };

    return factory;
  };
}
