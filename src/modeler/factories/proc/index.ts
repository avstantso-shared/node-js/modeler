import { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@types';
import Readable from './readable';
import ReadableNamed from './readable-named';
import Writable from '../writable';
import WritableNamed from './writable-named';
import ReadableWritable from './readable-writable';
import ReadableWritableNamed from './readable-writable-named';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>
>(factoryOptions: Modeler.Factories.Options.Proc<TModel, TModeler, TAccess>) {
  return {
    Readable: Readable<TModel, TModeler, TAccess>(factoryOptions),
    ReadableNamed: ReadableNamed<TModel, TModeler, TAccess>(factoryOptions),
    Writable: Writable<TModel, TModeler, TAccess>(factoryOptions),
    WritableNamed: WritableNamed<TModel, TModeler, TAccess>(factoryOptions),
    ReadableWritable: ReadableWritable<TModel, TModeler, TAccess>(
      factoryOptions
    ),
    ReadableWritableNamed: ReadableWritableNamed<TModel, TModeler, TAccess>(
      factoryOptions
    ),
  };
}
