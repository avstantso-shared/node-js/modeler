import { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@types';
import Extender from '../extender';
import Readable from './readable-named';
import Writable from './writable-named';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.Proc.Structure<
    TModel,
    TAccess
  > = Modeler.Factories.Proc.Structure<TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.Proc<TModel, TModeler, TAccess>
): Modeler.Factories.Base.Methods.ReadableWritableNamed<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const { extend, ...internalOptions } = factoryOptions;
  const readableFactory = Readable<TModel, TModeler, TAccess>(internalOptions);
  const writableFactory = Writable<TModel, TModeler, TAccess>(internalOptions);

  return (
    selection,
    insertProc,
    updateProc,
    deleteProc,
    deleteByAuthorProc
  ) => {
    const readable = readableFactory(selection);
    const writable = writableFactory(
      insertProc,
      updateProc,
      deleteProc,
      deleteByAuthorProc,
      (access) => readable(access).byId,
      (access) => readable(access).byName
    );

    const factory: Modeler.Access.For<
      Modeler.ReadableWritableNamed<TModel, TModeler>,
      TModel,
      TAccess
    > = (access) => {
      const r = readable(access);
      const w = writable(access);

      return Extender(extend, access, factory, { ...r, ...w });
    };

    return factory;
  };
}
