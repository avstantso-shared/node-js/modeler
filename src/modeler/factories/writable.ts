import type { ResultSetHeader, RowDataPacket } from 'mysql2/promise';
import Bluebird from 'bluebird';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Func, Proc } from '@avstantso/node-js--my-sql-wrapper';

import type * as Modeler from '@types';
import { MESSAGES, METHODS } from '@consts';
import { ModelError } from '@classes';
import { logMethod, idFromProc, Supports } from '@utils';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.Base.Structure<
    TModel,
    TAccess
  > = Modeler.Factories.Base.Structure<TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.Internal<TModel, TModeler, TAccess>
): Modeler.Factories.Base.Methods.Writable<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const { modelName, executor, idFromStr, isDetail, safe } = factoryOptions;
  const { call } = executor;

  return (insertProc, updateProc, deleteProc, deleteByAuthorProc, byId) => {
    const supports = Supports(modelName);

    return (access) => {
      function doInsert(entity: TModel['Insert'] & TModel['InsertSecrets']) {
        return logMethod
          .async(METHODS.insert)
          .then(() => insertProc({ access, entity }))
          .then((proc) => call<RowDataPacket[][]>(proc))
          .then(idFromProc);
      }

      const insertRaw: Modeler.Methods.Writable.Insert<
        TModel,
        TModeler
      >['raw'] = (entity) =>
        supports(insertProc, METHODS.insert) && safe(doInsert(entity));

      const insert: Modeler.Methods.Writable.Insert<TModel, TModeler> = (
        entity
      ) =>
        supports(insertProc, METHODS.insert) &&
        safe(doInsert(entity).then(byId(access)));
      insert.raw = insertRaw;

      function doUpdate(entity: TModel['Update']) {
        return logMethod
          .async(METHODS.update)
          .then(() => updateProc({ access, entity }))
          .then((proc) => call<RowDataPacket[][]>(proc))
          .then(idFromProc);
      }

      const updateRaw: Modeler.Methods.Writable.Update<
        TModel,
        TModeler
      >['raw'] = (entity) =>
        supports(insertProc, METHODS.insert) && safe(doUpdate(entity));

      const update: Modeler.Methods.Writable.Update<TModel, TModeler> = (
        entity
      ) =>
        supports(updateProc, METHODS.update) &&
        safe(doUpdate(entity).then(byId(access)));
      update.raw = updateRaw;

      const deleteById: Modeler.Methods.Writable.Delete<TModel, TModeler> = (
        id
      ) =>
        supports(deleteProc, METHODS.delete) &&
        safe(
          logMethod
            .async(METHODS.delete)
            .then(() =>
              deleteProc({
                access,
                id: id instanceof Func ? id : idFromStr(id),
              })
            )
            .then((proc) => call<RowDataPacket[][]>(proc))
            .then(idFromProc),
          // There is no such thing, okay
          ModelError.is
        );

      const deleteByAuthor: Modeler.Methods.Writable.Delete.ByAuthor<
        TModel,
        TModeler
      > = (author) =>
        supports(deleteByAuthorProc, METHODS.deleteByAuthor) &&
        safe(
          logMethod
            .async(METHODS.deleteByName)
            .then(() => deleteByAuthorProc({ access, author }))
            .then((proc) => call<RowDataPacket[][]>(proc))
            .then(([results1]) => {
              if (Array.isArray(results1)) {
                const { result } = Generics.Cast(results1[0][0]);
                return result;
              }

              const { affectedRows } = results1 as ResultSetHeader;
              return affectedRows;
            })
        );

      return {
        name: modelName,
        isDetail,
        insert,
        update,
        delete: deleteById,
        deleteByAuthor,
      };
    };
  };
}
