import { JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@types';

export default function <
  TResult extends Modeler.Union.ReadableOrReadableWritable<TModel, TModeler>,
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>
>(
  extend:
    | Modeler.Factories.Options.Extender.Provider<TModel, TModeler, TAccess>
    | Modeler.Factories.Options.Extender<TModel, TModeler, TAccess>,
  access: TAccess,
  factory: Modeler.Access.For<TResult, TModel, TAccess>,
  modeler: TResult
): TResult {
  const ex: Modeler.Factories.Options.Extender<TModel, TModeler, TAccess> =
    JS.is.function(extend) ? extend : extend?.extend;

  return (ex ? ex({ modeler, access, factory }) : modeler) as TResult;
}
