import View from './view';
import Proc from './proc';

export default {
  View,
  Proc,
};
