import Bluebird from 'bluebird';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  Conditions,
  ViewFields,
  injectCondition,
} from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import { METHODS } from '@consts';
import { ModelError } from '@classes';
import { logMethod } from '@utils';
import Writable from '../writable';

const { Eq } = Conditions;

export default function <
  TViewFields extends ViewFields,
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.View.Structure<
    TViewFields,
    TModel,
    TAccess
  > = Modeler.Factories.View.Structure<TViewFields, TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.View<
    TViewFields,
    TModel,
    TModeler,
    TAccess
  >
): Modeler.Factories.View.Methods.WritableNamed<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const writableFactory = Writable<TModel, TModeler, TAccess>(factoryOptions);
  const { executor, nameToId, safe, view } = factoryOptions;
  const { selectView } = executor;

  return (
    selection,
    insertProc,
    updateProc,
    deleteProc,
    deleteByAuthorProc,
    byId
  ) => {
    const writable = writableFactory(
      insertProc,
      updateProc,
      deleteProc,
      deleteByAuthorProc,
      byId
    );

    return (access) => {
      const w = writable(access);

      const deleteByName: Modeler.Methods.Writable.Delete.ByName<
        TModel,
        TModeler
      > = (name): Promise<Model.ID> =>
        safe(
          logMethod.async(METHODS.deleteByName).then(() => {
            return Bluebird.resolve(
              nameToId
                ? nameToId({ access, name })
                : selectView(view, (fields) => {
                    const { where } = injectCondition(
                      fields,
                      selection({ access }),
                      Eq(fields[Model.Fields.name], name)
                    );
                    return {
                      columns: [fields[Model.Fields.id]],
                      where,
                    };
                  }).then(([rows]) =>
                    rows.length ? rows[0][Model.Fields.id] : null
                  )
            ).then((idOrFunc) => (idOrFunc ? w.delete(idOrFunc) : null));
          }),
          // There is no such thing, okay
          ModelError.is
        );

      return { ...w, deleteByName };
    };
  };
}
