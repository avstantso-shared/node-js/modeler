import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { ViewFields } from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import Extender from '../extender';
import Readable from './readable-named';
import Writable from './writable-named';

export default function <
  TViewFields extends ViewFields,
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.View.Structure<
    TViewFields,
    TModel,
    TAccess
  > = Modeler.Factories.View.Structure<TViewFields, TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.View<
    TViewFields,
    TModel,
    TModeler,
    TAccess
  >
): Modeler.Factories.Base.Methods.ReadableWritableNamed<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const { extend, ...internalOptions } = factoryOptions;
  const readableFactory = Readable<TViewFields, TModel, TModeler, TAccess>(
    internalOptions
  );
  const writableFactory = Writable<TViewFields, TModel, TModeler, TAccess>(
    internalOptions
  );

  return (
    selection,
    insertProc,
    updateProc,
    deleteProc,
    deleteByAuthorProc
  ) => {
    const readable = readableFactory(selection);
    const writable = writableFactory(
      selection,
      insertProc,
      updateProc,
      deleteProc,
      deleteByAuthorProc,
      (access) => readable(access).byId
    );

    const factory: Modeler.Access.For<
      Modeler.ReadableWritableNamed<TModel, TModeler>,
      TModel,
      TAccess
    > = (access) => {
      const r = readable(access);
      const w = writable(access);

      return Extender(extend, access, factory, { ...r, ...w });
    };

    return factory;
  };
}
