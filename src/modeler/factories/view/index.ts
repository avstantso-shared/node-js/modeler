import { Model } from '@avstantso/node-or-browser-js--model-core';
import { ViewFields } from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import Readable from './readable';
import ReadableNamed from './readable-named';
import Writable from '../writable';
import WritableNamed from './writable-named';
import ReadableWritable from './readable-writable';
import ReadableWritableNamed from './readable-writable-named';

export default function <
  TViewFields extends ViewFields,
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>
>(
  factoryOptions: Modeler.Factories.Options.View<
    TViewFields,
    TModel,
    TModeler,
    TAccess
  >
) {
  return {
    Readable: Readable<TViewFields, TModel, TModeler, TAccess>(factoryOptions),
    ReadableNamed: ReadableNamed<TViewFields, TModel, TModeler, TAccess>(
      factoryOptions
    ),
    Writable: Writable<TModel, TModeler, TAccess>(factoryOptions),
    WritableNamed: WritableNamed<TViewFields, TModel, TModeler, TAccess>(
      factoryOptions
    ),
    ReadableWritable: ReadableWritable<TViewFields, TModel, TModeler, TAccess>(
      factoryOptions
    ),
    ReadableWritableNamed: ReadableWritableNamed<
      TViewFields,
      TModel,
      TModeler,
      TAccess
    >(factoryOptions),
  };
}
