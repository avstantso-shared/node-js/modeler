import Bluebird from 'bluebird';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  Conditions,
  ViewFields,
  injectCondition,
} from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import { METHODS } from '@consts';
import { logMethod, oneRow } from '@utils';
import Extender from '../extender';
import Readable from './readable';

const { Eq } = Conditions;

export default function <
  TViewFields extends ViewFields,
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.View.Structure<
    TViewFields,
    TModel,
    TAccess
  > = Modeler.Factories.View.Structure<TViewFields, TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.View<
    TViewFields,
    TModel,
    TModeler,
    TAccess
  >
): Modeler.Factories.Base.Methods.ReadableNamed<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  const { extend, ...internalOptions } = factoryOptions;
  const readableFactory = Readable<TViewFields, TModel, TModeler, TAccess>(
    internalOptions
  );

  const { executor, decorator, nameToId, safe, view } = factoryOptions;
  const { selectView } = executor;

  return (selection) => {
    const readable = readableFactory(selection);

    const factory: Modeler.Access.For<
      Modeler.ReadableNamed<TModel, TModeler>,
      TModel,
      TAccess
    > = (access) => {
      const r = readable(access);

      const byName: Modeler.Methods.Readable.ByName<TModel, TModeler> = (
        name
      ): Promise<TModel['Select']> =>
        safe(
          logMethod.async(METHODS.byName).then(() => {
            return nameToId
              ? r.byId(nameToId({ access, name }))
              : selectView(view, (fields) =>
                  injectCondition(
                    fields,
                    selection({ access }),
                    Eq(fields[Model.Fields.name], name)
                  )
                )
                  .then(oneRow<TModel['Select']>)
                  .then((row) => (decorator ? decorator(row) : row));
          })
        );

      return Extender(extend, access, factory, {
        ...r,
        byName,
      });
    };

    return factory;
  };
}
