import Bluebird from 'bluebird';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  Conditions,
  Func,
  ViewFields,
  injectCondition,
  Builder,
  GroupFunctions,
  As,
} from '@avstantso/node-js--my-sql-wrapper';
import type * as Modeler from '@types';
import { METHODS } from '@consts';
import { logMethod, oneRow } from '@utils';
import Extender from '../extender';

const { Eq } = Conditions;

export default function <
  TViewFields extends ViewFields,
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Modeler.Access.Union<TModel>,
  TFactory extends Modeler.Factories.View.Structure<
    TViewFields,
    TModel,
    TAccess
  > = Modeler.Factories.View.Structure<TViewFields, TModel, TAccess>
>(
  factoryOptions: Modeler.Factories.Options.View<
    TViewFields,
    TModel,
    TModeler,
    TAccess
  >
): Modeler.Factories.Base.Methods.Readable<
  TModel,
  TModeler,
  TAccess,
  TFactory
> {
  //#region internal types aliases
  type Select = TModel['Select'];
  type SelectArray = Select[];
  type PGResult = Model.Pagination.Result<TModel>;
  //#endregion

  const { modelName, executor, decorator, idFromStr, isDetail, safe, view } =
    factoryOptions;
  const { selectView } = executor;

  return (selection) => {
    const factory: Modeler.Access.For<
      Modeler.Readable<TModel, TModeler>,
      TModel,
      TAccess
    > = (access) => {
      const select: Modeler.Methods.Readable.Select<TModel, TModeler> = (
        search?
      ): Promise<SelectArray> =>
        safe(
          logMethod.async(METHODS.select).then(() =>
            selectView(view, selection({ access, search })).then(
              ([rawRows]) => {
                const rows = rawRows as SelectArray;
                return decorator ? rows.map(decorator) : rows;
              }
            )
          )
        );

      const selectPages: Modeler.Methods.Readable.Select<
        TModel,
        TModeler
      >['pages'] = (pagination, search?): Promise<PGResult> =>
        safe(
          logMethod.async(METHODS.select.pages).then(() => {
            const baseCallback = selection({ access, search });

            const totalCallback: Builder.SelectView.Callback<TViewFields> = (
              fields
            ) => {
              const colsOrStruct = baseCallback(fields);
              const { where }: Builder.SelectView.Callback.Result =
                Array.isArray(colsOrStruct)
                  ? { columns: colsOrStruct }
                  : colsOrStruct;

              return {
                columns: [As(GroupFunctions.Count(fields.id), 'total')],
                where,
              };
            };

            const rowsCallback: Builder.SelectView.Callback<TViewFields> = (
              fields
            ) => {
              const colsOrStruct = baseCallback(fields);
              const {
                orderBy,
                limit: _l,
                offset: _o,
                ...rest
              }: Builder.SelectView.Callback.Result = Array.isArray(
                colsOrStruct
              )
                ? { columns: colsOrStruct }
                : colsOrStruct;

              return {
                ...rest,
                orderBy: orderBy || [fields.putdate],
                limit: pagination.size,
                offset: pagination.size * pagination.num,
              };
            };

            return selectView(view, totalCallback)
              .then(oneRow<Model.Pagination.Result.Total>)
              .then(({ total }) =>
                selectView(view, rowsCallback).then(([rawRows]) => {
                  const rows = rawRows as SelectArray;
                  return [total, decorator ? rows.map(decorator) : rows];
                })
              );
          })
        );

      select.pages = selectPages;

      const byId: Modeler.Methods.Readable.ById<TModel, TModeler> = (id) =>
        safe(
          logMethod
            .async(METHODS.byId)
            .then(() => {
              return selectView(view, (fields) =>
                injectCondition(
                  fields,
                  selection({ access }),
                  Eq(
                    fields[Model.Fields.id as keyof TViewFields],
                    id instanceof Func ? id : idFromStr(id)
                  )
                )
              );
            })
            .then(oneRow<Select>)
            .then((row) => (decorator ? decorator(row) : row))
        );

      return Extender(factoryOptions, access, factory, {
        name: modelName,
        isDetail,
        select,
        byId,
      });
    };

    return factory;
  };
}
