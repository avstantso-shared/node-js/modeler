import { MESSAGES } from '@avstantso/node-or-browser-js--model-core';

const invalidMaster = 'Invalid master';
const notSupported = 'Not supported';
const noModelError = 'No model error';
const cannotDeleteOrUpdateAParentRow =
  'Cannot delete or update a parent row: a foreign key constraint fails';

export default {
  ...MESSAGES,
  invalidMaster,
  notSupported,
  noModelError,
  cannotDeleteOrUpdateAParentRow,
};
