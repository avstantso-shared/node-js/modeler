import { NamesTree } from '@avstantso/node-or-browser-js--utils';

const s = '';

export const METHODS = NamesTree.Names({
  select: { pages: s },
  byId: s,
  byName: s,
  insert: s,
  update: s,
  delete: s,
  deleteByAuthor: s,
  deleteByName: s,
}).Name;
