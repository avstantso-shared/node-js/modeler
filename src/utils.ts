import type { FieldPacket, RowDataPacket } from 'mysql2';
import Bluebird from 'bluebird';
import debug from 'debug';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  Arg,
  Condition,
  Conditions,
  Field,
} from '@avstantso/node-js--my-sql-wrapper';

import type * as Modeler from '@types';
import { MESSAGES } from '@consts';
import { ModelError } from '@classes';

const { And } = Conditions;

export type LogMethod = debug.Debugger & {
  async: (formatter: any, ...args: any[]) => Bluebird<void>;
};

export const logMethod: LogMethod = Generics.Cast(debug('modeler'));
logMethod.async = (formatter: any, ...args: any[]) =>
  Bluebird.resolve(logMethod(`${formatter}`, ...args));

export interface ModelAccessOverload {
  (login: string): Modeler.Access;
  <TModel extends Model.Structure>(
    login: string,
    master: TModel['Master']
  ): Modeler.Access;
}

export const ModelAccess: ModelAccessOverload = (
  login: string,
  master?: object
): Modeler.Access => ({ login, ...master });

export const oneRow = <T>([rows]: [RowDataPacket[], FieldPacket[]]) =>
  rows.length ? (rows[0] as T) : null;

export const oneRowProc = <T>([rowsP]: [RowDataPacket[][], FieldPacket[]]) =>
  rowsP[0].length ? (rowsP[0][0] as T) : null;

export const idFromProc = ([rowsP]: [RowDataPacket[][], FieldPacket[]]) =>
  (rowsP[0][0] as Model.IDed).id;

export function isReadableNamed<
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>
>(
  modeler: Modeler.Readable<TModel, TModeler>
): modeler is Modeler.ReadableNamed<TModel, TModeler> {
  return modeler && 'byName' in modeler;
}

export function isReadableWritable<
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>
>(
  modeler: Modeler.Readable<TModel, TModeler>
): modeler is Modeler.ReadableWritable<TModel, TModeler> {
  return modeler && 'delete' in modeler;
}

export function isReadableWritableNamed<
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>
>(
  modeler: Modeler.Readable<TModel, TModeler>
): modeler is Modeler.ReadableWritableNamed<TModel, TModeler> {
  return modeler && 'deleteByName' in modeler;
}

export function Supports(modelName: string) {
  return (method: Function, methodName: string) => {
    if (!method)
      throw new ModelError(modelName, methodName, MESSAGES.notSupported);

    return true;
  };
}

export namespace Search2Condition {
  export namespace Replacemaent {
    export type Item = (searchKey: string) => string;
  }

  export type Replacemaent = NodeJS.Dict<Replacemaent.Item>;
}

export function Search2Condition<TModel extends Model.Structure>(
  search: TModel['Condition'],
  replacemaent?: Search2Condition.Replacemaent
): Condition {
  const keys = Object.keys(search || {});
  if (!keys.length) return null;

  function resolve<T extends Arg.Type = Arg.Type>(key: string) {
    const value: T = JS.get.raw(search, key);
    const rp: Search2Condition.Replacemaent.Item =
      replacemaent && JS.get(replacemaent, key);

    return Conditions.Eq(Field.named(rp ? rp(key) : key), value);
  }

  if (1 === keys.length) return resolve(keys[0]);

  const [condition1, condition2, ...conditions]: Condition[] =
    keys.map(resolve);

  return Conditions.And(condition1, condition2, ...conditions);
}
