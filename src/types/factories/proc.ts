import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Proc, Arg } from '@avstantso/node-js--my-sql-wrapper';
import type { Access } from '../access';
import type * as Modeler from '../modeler';
import type * as Base from './base';

export interface Structure<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>
> extends Base.Structure<TModel, TAccess> {
  selection(props: {
    access: TAccess;
    pagination?: Model.Pagination;
    search?: TModel['Condition'];
    id?: Arg<any>; // byId case
    name?: Arg<any>; // byName case
  }): Proc;
}

export namespace Methods {
  export type WritableNamed<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Base.Structure<TModel, TAccess>
  > = (
    insertProc: Base.InsertProc<TModel, TAccess, TFactory>,
    updateProc: Base.UpdateProc<TModel, TAccess, TFactory>,
    deleteProc: Base.DeleteProc<TModel, TAccess, TFactory>,
    deleteByAuthorProc: Base.DeleteByAuthorProc<TModel, TAccess, TFactory>,
    byId: Access.For<
      Modeler.Methods.Readable.ById<TModel, TModeler>,
      TModel,
      TAccess
    >,
    byName: Access.For<
      Modeler.Methods.Readable.ByName<TModel, TModeler>,
      TModel,
      TAccess
    >
  ) => Access.For<Modeler.WritableNamed<TModel, TModeler>, TModel, TAccess>;
}

export interface Factory<
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Access.Union<TModel>,
  TFactory extends Base.Structure<TModel, TAccess>
> extends Omit<
    Base.Factory<TModel, TModeler, TAccess, TFactory>,
    'WritableNamed'
  > {
  WritableNamed: Methods.WritableNamed<TModel, TModeler, TAccess, TFactory>;
}
