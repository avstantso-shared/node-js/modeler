import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Arg, Proc } from '@avstantso/node-js--my-sql-wrapper';
import type { Access } from '../access';
import type * as Modeler from '../modeler';

export type SyncAsync<T> = T | Promise<T>;

export interface Structure<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>
> {
  selection: unknown;
  insertProc(props: {
    access: TAccess;
    entity: TModel['Insert'] & TModel['InsertSecrets'];
  }): SyncAsync<Proc>;
  updateProc?(props: {
    access: TAccess;
    entity: TModel['Update'];
  }): SyncAsync<Proc>;
  deleteProc?: (props: { access: TAccess; id: Arg<any> }) => SyncAsync<Proc>;
  deleteByAuthorProc?(props: {
    access: TAccess;
    author: string;
  }): SyncAsync<Proc>;
}

// #region factory types from structure
export type Selection<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>,
  TFactory extends Structure<TModel, TAccess>
> = TFactory['selection'];
export type InsertProc<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>,
  TFactory extends Structure<TModel, TAccess>
> = TFactory['insertProc'];
export type UpdateProc<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>,
  TFactory extends Structure<TModel, TAccess>
> = TFactory['updateProc'];
export type DeleteProc<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>,
  TFactory extends Structure<TModel, TAccess>
> = TFactory['deleteProc'];
export type DeleteByAuthorProc<
  TModel extends Model.Structure,
  TAccess extends Access.Union<TModel>,
  TFactory extends Structure<TModel, TAccess>
> = TFactory['deleteByAuthorProc'];
// #endregion

export namespace Methods {
  export type Readable<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Structure<TModel, TAccess>
  > = (
    selection: Selection<TModel, TAccess, TFactory>
  ) => Access.For<Modeler.Readable<TModel, TModeler>, TModel, TAccess>;
  export type ReadableNamed<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Structure<TModel, TAccess>
  > = (
    selection: Selection<TModel, TAccess, TFactory>
  ) => Access.For<Modeler.ReadableNamed<TModel, TModeler>, TModel, TAccess>;

  export type Writable<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Structure<TModel, TAccess>
  > = (
    insertProc: InsertProc<TModel, TAccess, TFactory>,
    updateProc: UpdateProc<TModel, TAccess, TFactory>,
    deleteProc: DeleteProc<TModel, TAccess, TFactory>,
    deleteByAuthorProc: DeleteByAuthorProc<TModel, TAccess, TFactory>,
    byId: Access.For<
      Modeler.Methods.Readable.ById<TModel, TModeler>,
      TModel,
      TAccess
    >
  ) => Access.For<Modeler.Writable<TModel, TModeler>, TModel, TAccess>;
  export type WritableNamed<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Structure<TModel, TAccess>
  > = (
    ...params: unknown[]
  ) => Access.For<Modeler.Writable<TModel, TModeler>, TModel, TAccess>;

  export type ReadableWritable<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Structure<TModel, TAccess>
  > = (
    selection: Selection<TModel, TAccess, TFactory>,
    insertProc: InsertProc<TModel, TAccess, TFactory>,
    updateProc: UpdateProc<TModel, TAccess, TFactory>,
    deleteProc: DeleteProc<TModel, TAccess, TFactory>,
    deleteByAuthorProc: DeleteByAuthorProc<TModel, TAccess, TFactory>
  ) => Access.For<Modeler.ReadableWritable<TModel, TModeler>, TModel, TAccess>;
  export type ReadableWritableNamed<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>,
    TFactory extends Structure<TModel, TAccess>
  > = (
    selection: Selection<TModel, TAccess, TFactory>,
    insertProc: InsertProc<TModel, TAccess, TFactory>,
    updateProc: UpdateProc<TModel, TAccess, TFactory>,
    deleteProc: DeleteProc<TModel, TAccess, TFactory>,
    deleteByAuthorProc: DeleteByAuthorProc<TModel, TAccess, TFactory>
  ) => Access.For<
    Modeler.ReadableWritableNamed<TModel, TModeler>,
    TModel,
    TAccess
  >;
}

export interface Factory<
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Access.Union<TModel>,
  TFactory extends Structure<TModel, TAccess>
> {
  Readable: Methods.Readable<TModel, TModeler, TAccess, TFactory>;
  ReadableNamed: Methods.ReadableNamed<TModel, TModeler, TAccess, TFactory>;

  Writable: Methods.Writable<TModel, TModeler, TAccess, TFactory>;
  WritableNamed: Methods.WritableNamed<TModel, TModeler, TAccess, TFactory>;

  ReadableWritable: Methods.ReadableWritable<
    TModel,
    TModeler,
    TAccess,
    TFactory
  >;
  ReadableWritableNamed: Methods.ReadableWritableNamed<
    TModel,
    TModeler,
    TAccess,
    TFactory
  >;
}
