import type {
  Model,
  DataDecorator,
} from '@avstantso/node-or-browser-js--model-core';
import type * as MySQLWrp from '@avstantso/node-js--my-sql-wrapper';
import type { Access } from '../access';
import type * as Modeler from '../modeler';
import type { ErrorHandler } from '../errorHandler';

export namespace Options {
  export interface NameToId<
    TModel extends Model.Structure,
    TAccess extends Access.Union<TModel>
  > {
    access: TAccess;
    name: MySQLWrp.Arg<string>;
  }

  export interface Detail {
    isDetail?: boolean;
  }

  export namespace Extender {
    export type Options<
      TModel extends Model.Structure,
      TModeler extends Modeler.Structure<TModel>,
      TAccess extends Access.Union<TModel>
    > = {
      modeler: Modeler.Union.ReadableOrReadableWritable<TModel, TModeler>;
      access: TAccess;
      factory: Access.For<
        Modeler.Union.ReadableOrReadableWritable<TModel, TModeler>,
        TModel,
        TAccess
      >;
    };

    export type Top<
      TModel extends Model.Structure,
      TModeler extends Modeler.Structure<TModel>
    > = Extender<TModel, TModeler, Access>;

    export type Detail<
      TModel extends Model.Structure,
      TModeler extends Modeler.Structure<TModel>
    > = Extender<TModel, TModeler, Access.Detail<TModel>>;

    export interface Provider<
      TModel extends Model.Structure,
      TModeler extends Modeler.Structure<TModel>,
      TAccess extends Access.Union<TModel>
    > {
      extend?: Extender<TModel, TModeler, TAccess>;
    }
  }

  export type Extender<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>
  > = (
    options: Extender.Options<TModel, TModeler, TAccess>
  ) => Modeler.Union.ReadableOrReadableWritable<TModel, TModeler>;

  export interface IdFromStr {
    idFromStr(id: MySQLWrp.Arg<Model.ID>): MySQLWrp.Arg<any>;
  }

  export interface IsSafe {
    isSafe?: boolean;
    errorHandler?: ErrorHandler;
  }

  export interface Safe {
    safe: <T>(
      promise: Promise<T>,
      filter?: (e: Error) => boolean
    ) => Promise<T>;
  }

  export type Set = MySQLWrp.Providers.PoolOrConn & IsSafe;
  export type External = MySQLWrp.Providers.Executor & IsSafe;

  export interface Common<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>
  > extends MySQLWrp.Providers.Executor,
      IsSafe,
      Extender.Provider<TModel, TModeler, TAccess> {
    modelName: string;
    decorator?: DataDecorator<TModel['Select']>;
    nameToId?(options: NameToId<TModel, TAccess>): MySQLWrp.Arg<Model.ID>;
  }

  export interface Internal<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>
  > extends Detail,
      Options<TModel, TModeler, TAccess>,
      Safe {}

  export interface View<
    TViewFields extends MySQLWrp.ViewFields,
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>
  > extends Internal<TModel, TModeler, TAccess> {
    view: MySQLWrp.View<TViewFields>;
  }

  export interface Proc<
    TModel extends Model.Structure,
    TModeler extends Modeler.Structure<TModel>,
    TAccess extends Access.Union<TModel>
  > extends Internal<TModel, TModeler, TAccess> {
    cdcRules?: MySQLWrp.ColDefConverter.Rule | MySQLWrp.ColDefConverter.Rule[];
  }
}

export interface Options<
  TModel extends Model.Structure,
  TModeler extends Modeler.Structure<TModel>,
  TAccess extends Access.Union<TModel>
> extends Options.IdFromStr,
    Options.Common<TModel, TModeler, TAccess> {}
