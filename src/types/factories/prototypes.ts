import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Options } from './options';
import type { Access } from '../access';

export type GetModelersSet<T> = (
  options: Options.Set
) => Access.For<T, never, Access>;
export type GetModeler<
  T,
  TModel extends Model.Structure = Model.Structure,
  TAccess extends Access.Union<TModel> = Access
> = (options: Options.External) => Access.For<T, TModel, TAccess>;
