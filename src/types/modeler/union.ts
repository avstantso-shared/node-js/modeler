import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure } from './structure';
import type * as RW from './readable-writable';

export type Readable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = RW.Readable<TModel, TModeler> | RW.ReadableNamed<TModel, TModeler>;

export type Writable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = RW.Writable<TModel, TModeler> | RW.WritableNamed<TModel, TModeler>;

export type ReadableWritable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> =
  | RW.ReadableWritable<TModel, TModeler>
  | RW.ReadableWritableNamed<TModel, TModeler>;

export type ReadableOrReadableWritable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = Readable<TModel, TModeler> | ReadableWritable<TModel, TModeler>;

export type Any<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> =
  | Readable<TModel, TModeler>
  | Writable<TModel, TModeler>
  | ReadableWritable<TModel, TModeler>;
