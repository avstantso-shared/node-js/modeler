import type { Arg } from '@avstantso/node-js--my-sql-wrapper';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

export interface Structure<T extends Model.Structure> {
  // readable
  select: {
    (search?: T['Condition']): Promise<T['Select'][]>;
    pages(
      pagination: Model.Pagination,
      search?: T['Condition']
    ): Promise<Model.Pagination.Result<T>>;
  };
  byId(id: Arg<Model.ID>): Promise<T['Select']>;

  // readable & named
  byName?(name: string): Promise<T['Select']>;

  // writable
  insert?: {
    (entity: T['Insert'] & T['InsertSecrets']): Promise<T['Select']>;
    raw(entity: T['Insert'] & T['InsertSecrets']): Promise<Model.ID>;
  };
  update?: {
    (entity: T['Update']): Promise<T['Select']>;
    raw(entity: T['Update']): Promise<Model.ID>;
  };
  delete?(id: Arg<Model.ID>): Promise<Model.ID>;
  deleteByAuthor?(author: string): Promise<number>;

  // writable & named
  deleteByName?(name: string): Promise<Model.ID>;

  // extended
  extended?: unknown;
}

export namespace Structure {
  export type Extender<
    T extends Model.Structure,
    TExtender extends object
  > = Structure<T> & { extended?: TExtender };
}
