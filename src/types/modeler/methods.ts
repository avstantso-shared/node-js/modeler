import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure } from './structure';

export namespace Readable {
  export type Select<
    TModel extends Model.Structure,
    TModeler extends Structure<TModel> = Structure<TModel>
  > = TModeler['select'];

  export type ById<
    TModel extends Model.Structure,
    TModeler extends Structure<TModel> = Structure<TModel>
  > = TModeler['byId'];

  export type ByName<
    TModel extends Model.Structure,
    TModeler extends Structure<TModel> = Structure<TModel>
  > = TModeler['byName'];
}

export namespace Writable {
  export type Insert<
    TModel extends Model.Structure,
    TModeler extends Structure<TModel> = Structure<TModel>
  > = TModeler['insert'];

  export type Update<
    TModel extends Model.Structure,
    TModeler extends Structure<TModel> = Structure<TModel>
  > = TModeler['update'];

  export type Delete<
    TModel extends Model.Structure,
    TModeler extends Structure<TModel> = Structure<TModel>
  > = TModeler['delete'];

  export namespace Delete {
    export type ByAuthor<
      TModel extends Model.Structure,
      TModeler extends Structure<TModel> = Structure<TModel>
    > = TModeler['deleteByAuthor'];

    export type ByName<
      TModel extends Model.Structure,
      TModeler extends Structure<TModel> = Structure<TModel>
    > = TModeler['deleteByName'];
  }
}
