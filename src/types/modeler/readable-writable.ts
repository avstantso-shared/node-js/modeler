import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure } from './structure';

export interface Metadata {
  readonly name: string;
  readonly isDetail: boolean;
}

export type Readable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = Metadata &
  Required<Pick<TModeler, 'select' | 'byId'>> &
  TModeler['extended'];

export type ReadableNamed<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = Readable<TModel, TModeler> & Required<Pick<TModeler, 'byName'>>;

export type Writable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = Metadata &
  Required<Pick<TModeler, 'insert' | 'update' | 'delete' | 'deleteByAuthor'>> &
  TModeler['extended'];

export type WritableNamed<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = Writable<TModel, TModeler> & Required<Pick<TModeler, 'deleteByName'>>;

export type ReadableWritable<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = Readable<TModel, TModeler> & Writable<TModel, TModeler>;

export type ReadableWritableNamed<
  TModel extends Model.Structure,
  TModeler extends Structure<TModel> = Structure<TModel>
> = ReadableNamed<TModel, TModeler> & WritableNamed<TModel, TModeler>;
