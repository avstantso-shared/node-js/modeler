import type { Model } from '@avstantso/node-or-browser-js--model-core';

export interface Access {
  readonly login: string;
}

export namespace Access {
  export type Detail<TModel extends Model.Structure> = Access &
    TModel['Master'];

  export type Union<TModel extends Model.Structure> =
    | Access
    | Access.Detail<TModel>;

  export type For<
    TResult,
    TModel extends Model.Structure = Model.Structure,
    TAccess extends Union<TModel> = Access
  > = (access: TAccess) => TResult;
}
