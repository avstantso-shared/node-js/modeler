export type ErrorHandler = (error: Error, context?: any) => any;
